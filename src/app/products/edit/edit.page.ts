import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { productService } from "../product.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { productos } from '../products.model';

@Component({
  selector: "app-edit",
  templateUrl: "./edit.page.html",
  styleUrls: ["./edit.page.scss"],
})
export class EditPage implements OnInit {
  product: productos;
  editProd: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProduct: productService,
    private router: Router
  ) {}

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('productId')) {
        return;
      }
      const productId = parseInt(paramMap.get('productId'));
      this.product = this.serviceProduct.getProduct(productId);

    });

    this.editProd = new FormGroup({
      pcantidad: new FormControl(this.product.candidad, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pcodigo: new FormControl(this.product.codigo, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      pnombre: new FormControl(this.product.nombre, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(3)],
      }),
      ppeso: new FormControl(this.product.peso, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pfecha_caducidad: new FormControl(this.product.fecha_caducidad, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescripcion: new FormControl(this.product.descripcion, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
    });

    this.editProd.value.pnombre = this.product.nombre;

  }

  editProduct() {

    if (!this.editProd.valid) {
      return;
    }

    this.serviceProduct.editProduct(
      this.editProd.value.pprecio,
      this.editProd.value.pcantidad,
      this.editProd.value.pcodigo,
      this.editProd.value.pnombre,
      this.editProd.value.ppeso,
      this.editProd.value.pfecha_caducidad,
      this.editProd.value.pdescripcion,

    );

    this.editProd.reset();
    
    this.router.navigate(['./products']);
    
  }
}