import { Component, Input, OnInit } from "@angular/core";
import { productService } from "./product.service";
import { proveedor, productos } from "./products.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "page-products",
  templateUrl: "./products.page.html",
  styleUrls: ["./products.page.scss"],
})
export class ProductsPage implements OnInit {
  @Input() nombre: string;
  products: productos[];
  constructor(
    private prodServ: productService,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    console.log("Carga inicial");
    this.products = this.prodServ.getAll();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.products = this.prodServ.getAll();
  }

  view(code: number) {
    this.router.navigate(["/products/detail/" + code]);
  }

  delete(code: number) {
    this.alertController
      .create({
        header: "Borrar Producto",
        message: "Esta seguro que desea borrar este producto?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.prodServ.deleteProduct(code);
              this.products = this.prodServ.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  update(code: number) {
    this.router.navigate(["/products/edit/" + code]);
  }

}
